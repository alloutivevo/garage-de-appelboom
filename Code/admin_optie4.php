<?php

	//database verbindingen configueren
	$hostname = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_name = "GaragedeAB";
	
	//verbinden met database
	$connection = mysqli_connect($hostname, $db_user, $db_pass, $db_name);
	
	//valideer sessie (admin)
	session_start(); 
	$email = $_SESSION['email'];
	if ($email == "admin"){
		echo "<script>console.log('Admin sessie actief');</script>";
	} else {
	echo "Ongeldige sessie.";
    echo "
	  
	  <script>
	  
	  geenemail = alert('Fout 403 Toegang geweigerd.');
	  if (geenemail = true){
				window.location.href = '/index.php'
			} else {
				window.location.href = '/index.php' }
			
	  </script>
	  
			";
	}
	
?>
<html>
	<head>
		<title>Garage de Appelboom</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Garage de Appelboom" >
		<meta name="keywords" content="autogarage, appelboom, reparatie, servicebeurt, service">
		<link rel="stylesheet" href="bootstrapreplace.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<link rel="shortcut icon" href="img/icon.png">
		<style>
		.container{
			border: 1px solid black;
			border-radius: 10px 10px 10px 10px;
			width:	40%;
			text-align:	center;
			background-color:	lightblue;
		}
		
		.button{
			width:	125px;
			height: 35px;
		}
		
		.veld{
			width:	70px;
		}
		
		hr{
			width:	80%;
		}
		</style>
		<script>
		
			//valideren welke afspraak aangevinkt is
			function validateBeurt() {
				if (document.reserveren.afspraak[0].checked == true) {
					var afspraak = document.reserveren.afspraak[0].value;
					console.log("Uw gekozen beurt is: " + afspraak);
				}
				else if (document.reserveren.afspraak[1].checked == true) {
					var afspraak = document.reserveren.afspraak[1].value;
					console.log("Uw gekozen beurt is: " + afspraak);
				}
				else {
					// LEEG
				}
			}
			
			//valideren welke monteur aangevinkt is
			function validateMonteur() {
				if (document.reserveren.monteur[0].checked == true) {
					var monteur = document.reserveren.monteur[0].value;
					console.log("Uw gekozen monteur is: " + monteur);
				}
				else if (document.reserveren.monteur[1].checked == true) {
					var monteur = document.reserveren.monteur[1].value;
					console.log("Uw gekozen monteur is: " + monteur);
				}
				else if (document.reserveren.monteur[2].checked == true) {
					var monteur = document.reserveren.monteur[2].value;
					console.log("Uw gekozen monteur is: " + monteur);
				}
				else {
					// LEEG
				}
			}
			
			function foutMelding() {
				document.getElementById("melding").innerHTML = "Er is iets mis gegaan";
			}
			
		</script>
	</head>
	<body>
		<!-- titel -->
		<h1 align="center">Garage de Appelboom</h1>
		
		<!-- inhoud -->
		<div class="container">
			<div class="row">
				<br>
				<form name="reserveren" enctype="multipart/form-data" method="POST" action="admin_optie4_stap2.php">
				
					<!-- klanten_id invullen -->
					<h3>Klanten_id</h3>
						
						<input type="number" class="veld" name="klanten_id" required>
						
					<hr>
					
				
					<!-- afspraak -->
					<h3>Afspraak</h3>
						
						<input type="radio" name="afspraak" id="afspraak" value="servicebeurt" onClick="validateBeurt();" required> Servicebeurt					
						<p><i>duurt ongeveer 1 uur</i></p>
						
						<input type="radio" name="afspraak" id="afspraak" value="reparatiebeurt" onClick="validateBeurt();"> Reparatiebeurt
						<p><i>duurt ongeveer halve dag</i></p>
					
					<hr>
					<!-- monteur -->
					<h3>Monteur</h3>
					
						<input type="radio" name="monteur" id="monteur" value="kees" onClick="validateMonteur();" required> Monteur Kees	<br><br>
						
						<input type="radio" name="monteur" id="monteur" value="piet" onClick="validateMonteur();"> Monteur Piet	<br><br>
					
						<input type="radio" name="monteur" id="monteur" value="henk" onClick="validateMonteur();"> Monteur Henk
						
					<hr>
					<!-- datum -->
					<h3>Afspraakdatum</h3>
					
					<span id="afspraakdatum"></span>
					
					<br><br>
				
					<input type="submit" class="button" value="Bevestigen" name="reserveren">
					<input type="button" class="button" value="Annuleren" name="Annuleren" onclick="location.href='index.php'">
					
					<br><br>
					
					<span id="melding"></span>
					
				</form>
				<br>
			</div>
		</div>
		<br>
	</body>
</html>
<script>

			//huidige datum als minimum voor date element
			function setDatum() {
			var vandaag = new Date();
		
			var jaar = vandaag.getFullYear(); 	// jaar
			var dag = vandaag.getDate(); 		// dag
			var maand = vandaag.getMonth() + 1; // maand
		
			if (maand < 10)
				maand = '0' + maand.toString();
		
			if (dag < 10)
				dag = '0' + dag.toString();
		
			var mindatum = jaar + '-' + maand + '-' + dag;
			
			document.getElementById("afspraakdatum").innerHTML = "<input type='date' name='afspraakdatum' id='afspraakdatum' min='" + mindatum + "' required>";
			}

			setDatum();

</script>