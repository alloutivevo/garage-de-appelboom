<?php

	//ontvangen van gegevens vanuit reservatie.php
	$afspraak		= $_POST["afspraak"];
	$monteur		= $_POST["monteur"];
	$afspraakdatum	= $_POST["afspraakdatum"];


	//database verbindingen configueren
	$hostname = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_name = "GaragedeAB";
	
	//verbinden met database
	$connection = mysqli_connect($hostname, $db_user, $db_pass, $db_name);
	
	//valideer sessie
	session_start(); 
	$email = $_SESSION['email'];
	if (!$email) {
	echo "Ongeldige sessie.";
    echo "
	  
	  <script>
	  
	  geenemail = alert('Sorry, pagina niet meer beschikbaar.');
	  if (geenemail = true){
				window.location.href = '/index.php'
			} else {
				window.location.href = '/index.php' }
			
	  </script>
	  
			";
	}
	
	//valideer klanten id voor in database
	$query = "SELECT id FROM klanten WHERE email = '$email'";
	$result = mysqli_query($connection, $query);
	$klanten_id = mysqli_fetch_row($result);
	$klanten_id = $klanten_id[0];
	echo "<script>console.log('Uw klanten_id is: $klanten_id');</script>";
	
	//ophalen van tijden
	$query = "SELECT afspraaktijd FROM bestellingen WHERE afspraak = '$afspraak' AND monteur = '$monteur' AND afspraakdatum = '$afspraakdatum'";
	$result = mysqli_query($connection, $query);
	$gegevens = mysqli_fetch_row($result);
	
?>
<html>
	<head>
		<title>Garage de Appelboom</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Garage de Appelboom" >
		<meta name="keywords" content="autogarage, appelboom, reparatie, servicebeurt, service">
		<link rel="stylesheet" href="bootstrapreplace.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<link rel="shortcut icon" href="img/icon.png">
		<style>
		.container{
			border: 1px solid black;
			border-radius: 10px 10px 10px 10px;
			width:	40%;
			text-align:	center;
			background-color:	#ffffcc;
		}
		
		.button{
			width:	125px;
			height: 35px;
		}
		
		.veld{
			width:	200px;
		}
		
		hr{
			width:	80%;
		}
		</style>
		<script>
			
			function succesMelding() {
				alert("Een bevestiging van de afspraak is verzonden naar uw e-mail");
				document.getElementById("melding").innerHTML = "Afspraak gemaakt!<br> <a href='/home.php'>Klik hier om terug naar de home-page te gaan</a>";
			}
			
			function foutMelding() {
				document.getElementById("melding").innerHTML = "Er is iets mis gegaan.";
			}
			
		</script>
	</head>
	<body>
		<!-- titel -->
		<h1 align="center">Garage de Appelboom</h1>
		
		<!-- inhoud -->
		<div class="container">
			<div class="row">
				<br>
				<form name="reserveren_stap2" enctype="multipart/form-data" method="POST" action="">
				
					<?php
						echo "
						
							<input type='hidden' name='afspraak' 		value='$afspraak'>
							<input type='hidden' name='monteur' 		value='$monteur'>
							<input type='hidden' name='afspraakdatum'	value='$afspraakdatum'>
						
						";
					?>
				
					<!-- tijdstip -->
					<h3>Kies een tijdstip</h3>
					<?php echo "<p><i>U heeft gekozen voor de afspraak $afspraak met monteur $monteur op $afspraakdatum</i></p>"; ?>
					
					
					<hr>
				
					
					<?php
					
					if($afspraak == "servicebeurt"){
						
						if($gegevens == 0){
							echo "
							
							<select name='tijd' required>
								<option value=''>Tijd</option>
								<option value='08:00'>08:00</option>
								<option value='09:00'>09:00</option>
								<option value='10:00'>10:00</option>
								<option value='11:00'>11:00</option>
								<option value='12:30'>12:30</option>
								<option value='13:30'>13:30</option>
								<option value='14:30'>14:30</option>
								<option value='15:30'>15:30</option>
								<option value='16:00'>16:00</option>
							</select>
							
							";
						} else {
						include "sb_tijden.php";
						}
						
					} else {
						
						if($gegevens == 0){
							echo "
							
							<select name='tijd' required>
								<option value=''>Tijd</option>
								<option value='08:00'>08:00</option>
								<option value='12:30'>12:30</option>
							</select>
							
							";
						} else {
						include "rb_tijden.php";
						}
						
					}
					
					?>
					
					<br><br>
					
					<p>Pauze ingepland van 12:00 tot 12:30</p>
					
					<input type="submit" class="button" value="Bevestigen" name="reserveren_stap2">
					<input type="button" class="button" value="Terug" name="Terug" onclick="location.href='reservatie.php'">
					
					<br><br>
					
					<span id="melding"></span>
					
				</form>
				<br>
			</div>
		</div>
		<br>
	</body>
</html>
<?php

	if(isset($_POST['reserveren_stap2'])){
		
		$afspraaktijd = $_POST["tijd"];
		
		$query = "SELECT * FROM bestellingen WHERE afspraak = '$afspraak' AND monteur = '$monteur' AND afspraakdatum = '$afspraakdatum' AND afspraaktijd = '$afspraaktijd'";
		$result = mysqli_query($connection, $query);
		
		if (mysqli_num_rows($result) > 0){
			
			echo "<script>	foutMelding();	</script>";
			
		} else {
			
			$query = "INSERT INTO bestellingen (klanten_id, afspraak, monteur, afspraakdatum, afspraaktijd) 
      	    	  VALUES ('$klanten_id', '$afspraak', '$monteur', '$afspraakdatum', '$afspraaktijd')";
			$results = mysqli_query($connection, $query);
			
			echo "<script>	succesMelding();	</script>";
			
		}
		
	}
?>