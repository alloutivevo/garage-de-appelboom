<?php

	//database verbindingen configueren
	$hostname = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_name = "GaragedeAB";
	
	//connecting to database
	$connection = mysqli_connect($hostname, $db_user, $db_pass, $db_name);
	if (mysqli_connect_errno()){
		die("Error connecting to the db");
	}
	
	//valideer sessie (admin)
	session_start(); 
	$email = $_SESSION['email'];
	if ($email == "admin"){
		echo "<script>console.log('Admin sessie actief');</script>";
	} else {
	echo "Ongeldige sessie.";
    echo "
	  
	  <script>
	  
	  geenemail = alert('Fout 403 Toegang geweigerd.');
	  if (geenemail = true){
				window.location.href = '/index.php'
			} else {
				window.location.href = '/index.php' }
			
	  </script>
	  
			";
	}
	
	//opvragen van klanten
	$query = "SELECT * FROM klanten";
	$result = mysqli_query($connection, $query);

?>
<html>
	<head>
		<title>Garage de Appelboom</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Garage de Appelboom" >
		<meta name="keywords" content="autogarage, appelboom, reparatie, servicebeurt, service">
		<link rel="stylesheet" href="bootstrapreplace.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<link rel="shortcut icon" href="img/icon.png">
		<style>
		.container{
			border: 1px solid black;
			border-radius: 10px 10px 10px 10px;
			width:	40%;
			text-align:	center;
			background-color:	lightblue;
		}
		
		.button{
			width:	125px;
			height: 35px;
		}
		
		.veld{
			width:	200px;
		}
		
		ol{
			text-align:	center;
		}
		
		hr{
			width:	80%;
		}
		
		td{
			text-align:	center;
			border-style:	solid;
			background-color:	#FFFFFF;
			width:	150px;
		}
		</style>
	</head>
	<body>
		<!-- titel -->
		<h1 align="center">Garage de Appelboom</h1>
		
		<!-- inhoud -->
		<div class="container">
			<div class="row">
				<br>
				
				<h3>Klanten van GaragedeAB</h3>
				
				<br>
				
				<table>
				
					<tr>
						<td><b> ID </b></td>
						<td><b> NAAM </b></td>
						<td><b> KENTEKEN </b></td>
						<td><b> E-MAIL </b></td>
						<td><b> BASE64	</b></td>
					</tr>
				
					<?php
					while($gegevens = mysqli_fetch_row($result)){
		
						$id 		= $gegevens[0];
						$naam		= $gegevens[1];
						$kenteken	= $gegevens[2];
						$email		= $gegevens[3];
						$wachtwoord	= $gegevens[4];

							echo "<tr>";
								echo "<td> $id </td>";
								echo "<td> $naam </td>";
								echo "<td> $kenteken </td>";
								echo "<td> $email </td>";
								echo "<td> $wachtwoord </td>";	
							echo "</tr>";
					}
					?>

				</table>
				
				<br><br>
				
				<input type="button" class="button" value="Terug" name="Terug" onclick="location.href='admin_home.php'">
				
				<br><br>

			</div>
		</div>
	</body>
</html>