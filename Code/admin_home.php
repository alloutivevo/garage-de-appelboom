<?php

	//database verbindingen configueren
	$hostname = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_name = "GaragedeAB";
	
	//connecting to database
	$connection = mysqli_connect($hostname, $db_user, $db_pass, $db_name);
	if (mysqli_connect_errno()){
		die("Error connecting to the db");
	}
	
	//valideer sessie (admin)
	session_start(); 
	$email = $_SESSION['email'];
	if ($email == "admin"){
		echo "<script>console.log('Admin sessie actief');</script>";
	} else {
	echo "Ongeldige sessie.";
    echo "
	  
	  <script>
	  
	  geenemail = alert('Fout 403 Toegang geweigerd.');
	  if (geenemail = true){
				window.location.href = '/index.php'
			} else {
				window.location.href = '/index.php' }
			
	  </script>
	  
			";
	}
	
	//naam opvragen van gebruiker
	$query = "SELECT naam FROM klanten WHERE email = '$email'";
	$result = mysqli_query($connection, $query);
	$gegevens = mysqli_fetch_row($result);

?>
<html>
	<head>
		<title>Garage de Appelboom</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Garage de Appelboom" >
		<meta name="keywords" content="autogarage, appelboom, reparatie, servicebeurt, service">
		<link rel="stylesheet" href="bootstrapreplace.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<link rel="shortcut icon" href="img/icon.png">
		<style>
		.container{
			border: 1px solid black;
			border-radius: 10px 10px 10px 10px;
			width:	40%;
			text-align:	center;
			background-color:	lightblue;
		}
		
		.button{
			width:	125px;
			height: 35px;
		}
		
		.veld{
			width:	200px;
		}
		
		ol{
			text-align:	center;
		}
		
		hr{
			width:	80%;
		}
		</style>
	</head>
	<body>
		<!-- titel -->
		<h1 align="center">Garage de Appelboom</h1>
		
		<!-- inhoud -->
		<div class="container">
			<div class="row">
				<br>
				<form action="uitloggen.php" method="POST">
					<h3>Welkom <?php echo $gegevens[0]; ?></h3>
					
					<br>
						<ol>
							<a href="admin_optie1.php">	<li>Producten aanpassen</li>	</a>
								<br>
							<a href="admin_optie2.php">	<li>Bestellingen weergeven</li>	</a>
								<br>
							<a href="admin_optie3.php">	<li>Klanten weergeven</li>		</a>
								<br>
							<a href="admin_optie4.php">	<li>Afspraak maken</li>			</a>
						</ol>
					
					<hr>
					
					<input type="button" class="button" value="Reserveren" name="reservatie" onclick="location.href='reservatie.php'">
					<input type="button" class="button" value="Instellingen" name="instellingen" onclick="location.href='instellingen.php'">
					
					<br><br>
					
					
					<input type="submit" class="button" value="Uitloggen" name="Uitloggen" onclick="location.href='uitloggen.php'">
				</form>
				<br>
			</div>
		</div>
	</body>
</html>