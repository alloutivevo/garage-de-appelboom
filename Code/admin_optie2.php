<?php

	//database verbindingen configueren
	$hostname = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_name = "GaragedeAB";
	
	//connecting to database
	$connection = mysqli_connect($hostname, $db_user, $db_pass, $db_name);
	if (mysqli_connect_errno()){
		die("Error connecting to the db");
	}
	
	//valideer sessie (admin)
	session_start(); 
	$email = $_SESSION['email'];
	if ($email == "admin"){
		echo "<script>console.log('Admin sessie actief');</script>";
	} else {
	echo "Ongeldige sessie.";
    echo "
	  
	  <script>
	  
	  geenemail = alert('Fout 403 Toegang geweigerd.');
	  if (geenemail = true){
				window.location.href = '/index.php'
			} else {
				window.location.href = '/index.php' }
			
	  </script>
	  
			";
	}
	
	//opvragen van producten
	$query = "SELECT * FROM bestellingen";
	$result = mysqli_query($connection, $query);

?>
<html>
	<head>
		<title>Garage de Appelboom</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Garage de Appelboom" >
		<meta name="keywords" content="autogarage, appelboom, reparatie, servicebeurt, service">
		<link rel="stylesheet" href="bootstrapreplace.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<link rel="shortcut icon" href="img/icon.png">
		<style>
		.container{
			border: 1px solid black;
			border-radius: 10px 10px 10px 10px;
			width:	80%;
			text-align:	center;
			background-color:	lightblue;
		}
		
		.button{
			width:	125px;
			height: 35px;
		}
		
		.buttongr{
			width:	180px;
			height: 35px;
		}
		
		.veld{
			width:	100px;
		}
		
		.veldkl{
			width:	70px;
		}
		
		ol{
			text-align:	center;
		}
		
		hr{
			width:	80%;
		}
		
		td{
			text-align:	center;
			border-style:	solid;
			background-color:	#FFFFFF;
			width:	150px;
		}
		</style>
	</head>
	<body>
		<!-- titel -->
		<h1 align="center">Garage de Appelboom</h1>
		
		<!-- inhoud -->
		<div class="container">
			<div class="row">
				<br>
				
				<h3>Bestellingen van GaragedeAB</h3>
				
				<br>
				
				<div>
					<table>
				
						<tr>
							<td><b> ID </b></td>
							<td><b> KLANTEN_ID </b></td>
							<td><b> PRODUCT_ID </b></td>
							<td><b> BESTELLINGDATUM </b></td>
							<td><b> TOTAALPRIJS </b></td>
							<td><b> AFSPRAAK </b></td>
							<td><b> MONTEUR </b></td>
							<td><b> AFSPRAAKDATUM </b></td>
							<td><b> AFSPRAAKTIJD </b></td>
						</tr>
					
					<?php
					while($gegevens = mysqli_fetch_row($result)){
		
						$id 			= $gegevens[0];
						$klanten_id		= $gegevens[1];
						$product_id		= $gegevens[2];
						$bestellingdatum = $gegevens[3];
						$totaalprijs 	= $gegevens[4];
						$afspraak 		= $gegevens[5];
						$monteur		= $gegevens[6];
						$afspraakdatum	= $gegevens[7];
						$afspraaktijd	= $gegevens[8];

							echo "<tr>";
								echo "<td> $id </td>";
								echo "<td> $klanten_id </td>";
								echo "<td> $product_id </td>";
								echo "<td> $bestellingdatum </td>";
								echo "<td> $totaalprijs </td>";
								echo "<td> $afspraak </td>";
								echo "<td> $monteur </td>";
								echo "<td> $afspraakdatum </td>";
								echo "<td> $afspraaktijd </td>";
							echo "</tr>";
					}
					?>

					</table>
				</div>
				
			</div>
			<div class="row"><hr>	
				
				<div class="col-sm-6">
				<!-- eerste form - producten toevoegen -->
				<form name="toevoegen" method="POST">
					
					<h4>Product toevoegen aan bestellingen</h4>
					
					<input type="number" placeholder="ID" name="id" class="veldkl" required>
					<input type="number" placeholder="product_id" name="product_id" style="width: 100px;" required>
					
					<br><br>
					
					<input type="submit" class="buttongr" value="Product toevoegen" name="toevoegen">
				</form>
				</div>
				
				
				
				<div class="col-sm-6">
				<!-- tweede form - producten verwijderen -->
				<form name="bekijken" method="POST" action="/pdf/index.php">
					
					<h4>Bestelling weergeven PDF</h4>
					
					<input type="number" placeholder="id" name="id" class="veldkl" required>
					
					<br><br>
					
					<input type="submit" class="buttongr" value="Bestelling weergeven PDF" name="bekijken">
				</form>
				</div>
				
			</div><hr>
				
				<input type="button" class="button" value="Terug" name="Terug" onclick="location.href='admin_home.php'">
				
				<br><br>

			</div>
		</div>
	</body>
</html>
<?php
	
	if(isset($_POST["toevoegen"])){
		
		$id = $_POST["id"];
		$product_id = $_POST["product_id"];
		
		$query = "UPDATE bestellingen SET product_id = '$product_id' WHERE id = '$id'";
				  
		$results = mysqli_query($connection, $query);
		
		header("location: admin_optie2.php");
		
	}
	
	

?>