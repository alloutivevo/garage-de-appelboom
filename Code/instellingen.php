<?php

	//database verbindingen configueren
	$hostname = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_name = "GaragedeAB";
	
	//verbinding naar database
	$connection = mysqli_connect($hostname, $db_user, $db_pass, $db_name);
	if (mysqli_connect_errno()){
		die("Error connecting to the db");
	}
	
	//valideer sessie
	session_start(); 
	$email = $_SESSION['email'];
	if (!$email) {
	echo "Ongeldige sessie.";
    echo "
	  
	  <script>
	  
	  geenemail = alert('Sorry, pagina niet meer beschikbaar.');
	  if (geenemail = true){
				window.location.href = '/index.php'
			} else {
				window.location.href = '/index.php' }
			
	  </script>
	  
			";
	}
	
	//informatie opvragen van gebruiker
	$query = "SELECT naam,kenteken,email,wachtwoord FROM klanten WHERE email = '$email'";
	$result = mysqli_query($connection, $query);
	$gegevens = mysqli_fetch_row($result);
	$gegevens[3] = base64_decode($gegevens[3]);
	
	$sessionemail = $email;
	
	//opdrachten om gegevens te wijzigen
	if(isset($_POST['instellingen'])){
		
		// email wijzigen is uitgeschakeld

		//ingevulde gegevens valideren
		$naam = mysqli_real_escape_string($connection, $_POST["naam"]);
		$kenteken = mysqli_real_escape_string($connection, $_POST["kenteken"]);
		$wachtwoord = mysqli_real_escape_string($connection, $_POST["wachtwoord"]);  
		$wachtwoord = base64_encode($wachtwoord);
		
			
		//verschillende opdrachten
		$update_naam = "UPDATE klanten SET naam = '$naam' WHERE email = '$sessionemail'";
		$update_kenteken = "UPDATE klanten SET kenteken = '$kenteken' WHERE email = '$sessionemail'";
		$update_wachtwoord = "UPDATE klanten SET wachtwoord = '$wachtwoord' WHERE email = '$sessionemail'";
		
		//opdrachten uitvoeren
		$opdracht_naam = mysqli_query($connection, $update_naam);
		$opdracht_kenteken = mysqli_query($connection, $update_kenteken);
		$opdracht_wachtwoord = mysqli_query($connection, $update_wachtwoord);
		
		//proces afronden
		echo "
			<script>
			succes = alert('Gegevens gewijzigd.');
			if (succes = true){
				window.location.href = '/index.php'
			}
			</script>
			";
	}
?>
<html>
	<head>
		<title>Garage de Appelboom</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Garage de Appelboom" >
		<meta name="keywords" content="autogarage, appelboom, reparatie, servicebeurt, service">
		<link rel="stylesheet" href="bootstrapreplace.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<link rel="shortcut icon" href="img/icon.png">
		<style>
		.container{
			border: 1px solid black;
			border-radius: 10px 10px 10px 10px;
			width:	40%;
			text-align:	center;
			background-color:	#ffffcc;
		}
		
		.button{
			width:	125px;
			height: 35px;
		}
		
		.veld{
			width:	200px;
		}
		</style>
	</head>
	<body>
		<!-- titel -->
		<h1 align="center">Garage de Appelboom</h1>
		
		<!-- inhoud -->
		<div class="container">
			<div class="row">
				<br>
				<form name="instellingen" enctype="multipart/form-data" method="POST" action="">
				
					<h3>Wijzig uw gegevens</h3>
					<br>
					<h4>Persoonlijk</h4>
				
					<p>Volledige naam</p>
					<input value="<?php echo $gegevens[0]; ?>" type="text" class="veld" name="naam" maxlength="50" required>
				
					<br><br>
				
					<p>Kenteken</p>
					<input value="<?php echo $gegevens[1]; ?>" type="text" class="veld" name="kenteken" maxlength="10" required>
				
					<br><br>
					<br>
					<h4>Beveiliging</h4>
				
					<p>E-mail adres</p>
					<input value="<?php echo $gegevens[2]; ?>" type="email" class="veld" name="email" maxlength="50" disabled>
				
					<br><br>
					
					<p>Wachtwoord</p>
					<input value="<?php echo $gegevens[3]; ?>" type="text" class="veld" name="wachtwoord" maxlength="30" required>
				
					<br><br>
					
					<input type="submit" class="button" value="Opslaan" name="instellingen">
					<input type="button" class="button" value="Annuleren" name="annuleren" onclick="location.href='home.php'">
				</form>
				<br>
			</div>
		</div>
	</body>
</html>