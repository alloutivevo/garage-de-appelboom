<?php

	//database verbindingen configueren
	$hostname = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_name = "GaragedeAB";
	
	//connecting to database
	$connection = mysqli_connect($hostname, $db_user, $db_pass, $db_name);
	if (mysqli_connect_errno()){
		die("Error connecting to the db");
	}
	
	//valideer sessie (admin)
	session_start(); 
	$email = $_SESSION['email'];
	if ($email == "admin"){
		echo "<script>console.log('Admin sessie actief');</script>";
	} else {
	echo "Ongeldige sessie.";
    echo "
	  
	  <script>
	  
	  geenemail = alert('Fout 403 Toegang geweigerd.');
	  if (geenemail = true){
				window.location.href = '/index.php'
			} else {
				window.location.href = '/index.php' }
			
	  </script>
	  
			";
	}
	
	//opvragen van producten
	$query = "SELECT * FROM product";
	$result = mysqli_query($connection, $query);

?>
<html>
	<head>
		<title>Garage de Appelboom</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Garage de Appelboom" >
		<meta name="keywords" content="autogarage, appelboom, reparatie, servicebeurt, service">
		<link rel="stylesheet" href="bootstrapreplace.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<link rel="shortcut icon" href="img/icon.png">
		<style>
		.container{
			border: 1px solid black;
			border-radius: 10px 10px 10px 10px;
			width:	40%;
			text-align:	center;
			background-color:	lightblue;
		}
		
		.button{
			width:	125px;
			height: 35px;
		}
		
		.buttongr{
			width:	180px;
			height: 35px;
		}
		
		.veld{
			width:	100px;
		}
		
		.veldkl{
			width:	70px;
		}
		
		ol{
			text-align:	center;
		}
		
		hr{
			width:	80%;
		}
		
		td{
			text-align:	center;
			border-style:	solid;
			background-color:	#FFFFFF;
			width:	150px;
		}
		</style>
	</head>
	<body>
		<!-- titel -->
		<h1 align="center">Garage de Appelboom</h1>
		
		<!-- inhoud -->
		<div class="container">
			<div class="row">
				<br>
				
				<h3>Producten van GaragedeAB</h3>
				
				<br>
				
				<!-- leeg gedeelte links -->
				<div class="col-sm-4">
				</div>
				
				<div class="col-sm-4">
					<table>
				
						<tr>
							<td><b> ID </b></td>
							<td><b> NAAM </b></td>
							<td><b> PRIJS </b></td>
						</tr>
					
					<?php
					while($gegevens = mysqli_fetch_row($result)){
		
						$id 		= $gegevens[0];
						$naam		= $gegevens[1];
						$prijs		= $gegevens[2];

							echo "<tr>";
								echo "<td> $id </td>";
								echo "<td> $naam </td>";
								echo "<td> $prijs </td>";
							echo "</tr>";
					}
					?>

					</table>
				</div>
				
				<!-- leeg gedeelte rechts -->
				<div class="col-sm-4">
				</div>
				
			</div>
			<div class="row"><hr>	
				
				<div class="col-sm-6">
				<!-- eerste form - producten toevoegen -->
				<form name="toevoegen" method="POST">
					
					<h4>Producten toevoegen</h4>
					
					<input type="text" placeholder="naam" name="naam" class="veld" required>
					<input type="number" placeholder="prijs" name="prijs" class="veldkl" required>
					
					<br><br>
					
					<input type="submit" class="buttongr" value="Producten toevoegen" name="toevoegen">
				</form>
				</div>
				
				
				
				<div class="col-sm-6">
				<!-- tweede form - producten verwijderen -->
				<form name="verwijderen" method="POST">
					
					<h4>Producten verwijderen</h4>
					
					<input type="number" placeholder="id" name="id" class="veldkl" required>
					
					<br><br>
					
					<input type="submit" class="buttongr" value="Producten verwijderen" name="verwijderen">
				</form>
				</div>
				
			</div><hr>
				
				<input type="button" class="button" value="Terug" name="Terug" onclick="location.href='admin_home.php'">
				
				<br><br>

			</div>
		</div>
	</body>
</html>
<?php
	
	if(isset($_POST["toevoegen"])){
		
		$naam = $_POST["naam"];
		$prijs = $_POST["prijs"];
		
		$query = "INSERT INTO product (naam, prijs) 
      	    	  VALUES ('$naam', '$prijs')";
				  
		$results = mysqli_query($connection, $query);
		
		header("location: admin_optie1.php");
		
	}
	
	if(isset($_POST["verwijderen"])){
		
		$id = $_POST["id"];
		
		$query = "DELETE FROM product WHERE id = '$id'";
		
		$results = mysqli_query($connection, $query);
		
		header("location: admin_optie1.php");
		
	}

?>