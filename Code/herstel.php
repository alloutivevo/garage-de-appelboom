<?php

	//database verbindingen configueren
	$hostname = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_name = "GaragedeAB";
	
	//verbinden met database
	$connection = mysqli_connect($hostname, $db_user, $db_pass, $db_name);
	
	//valideer sessie
	session_start();
	if(isset($_SESSION['email'])){
		header("location: home.php");
		echo "<script>console.log('sessie actief');</script>";
	} else {
		echo "<script>console.log('sessie onactief');</script>";
	}
	
	//herstellen
	if(isset($_POST['herstel'])){		
           $email = mysqli_real_escape_string($connection, $_POST["email"]);  
           $kenteken = mysqli_real_escape_string($connection, $_POST["kenteken"]);  
           $query = "SELECT * FROM klanten WHERE email = '$email' AND kenteken = '$kenteken'";  
           $result = mysqli_query($connection, $query);  
           if(mysqli_num_rows($result) > 0)  
           {  
			echo "
				<script>
					herstelsucces = alert('Herstel e-mail verzonden naar: $email');
					window.location.href = '/index.php'
				</script>
			";
		} else {
			echo "<script>alert('Ongeldige gegevens');</script>";
		}
	}
?>
<html>
	<head>
		<title>Garage de Appelboom</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Garage de Appelboom" >
		<meta name="keywords" content="autogarage, appelboom, reparatie, servicebeurt, service">
		<link rel="stylesheet" href="bootstrapreplace.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<link rel="shortcut icon" href="img/icon.png">
		<style>
		.container{
			border: 1px solid black;
			border-radius: 10px 10px 10px 10px;
			width:	40%;
			text-align:	center;
			background-color:	#ffffcc;
		}
		
		.button{
			width:	125px;
			height: 35px;
		}
		
		.veld{
			width:	200px;
		}
		</style>
	</head>
	<body>
		<!-- titel -->
		<h1 align="center">Garage de Appelboom</h1>
		
		<!-- inhoud -->
		<div class="container">
			<div class="row">
				<br>
				<form name="herstel" enctype="multipart/form-data" method="POST" action="">
				
					<h4>Gelieve hieronder uw gegevens invullen</h4>
					
					<br>
				
					<p>E-mail adres</p>
					<input type="email" class="veld" name="email" maxlength="50" required>
				
					<br><br>
				
					<p>Kenteken</p>
					<input type="text" class="veld" name="kenteken" maxlength="10" required>
				
					<br><br>
				
					<input type="submit" class="button" value="Bevestigen" name="herstel">
					<input type="button" class="button" value="Annuleren" name="Annuleren" onclick="location.href='index.php'">
				</form>
				<br>
			</div>
		</div>
	</body>
</html>