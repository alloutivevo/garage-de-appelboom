<?php
	
	//database verbindingen configueren
	$hostname = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_name = "GaragedeAB";
	
	//connecting to database
	$connection = mysqli_connect($hostname, $db_user, $db_pass, $db_name);
	if (mysqli_connect_errno()){
		die("Error connecting to the db");
	}
	
	$id = $_POST["id"];
	
	//querys voor bestellingen
	$query_bestellingen = "SELECT * FROM bestellingen WHERE id = '$id'";
	$result_bestellingen = mysqli_query($connection, $query_bestellingen);
	$gegevens_bestellingen = mysqli_fetch_row($result_bestellingen);
	//columns -> id, klanten_id, product_id, bestellingendatum, totaalprijs, afspraak, monteur, afspraakdatum, afspraaktijd
	
	
	//querys voor klanten
	$query_klanten = "SELECT * FROM klanten WHERE id = '$gegevens_bestellingen[1]'";
	$result_klanten = mysqli_query($connection, $query_klanten);
	$gegevens_klanten = mysqli_fetch_row($result_klanten);
	//columns -> id, naam, kenteken, email, wachtwoord
	
	//querys voor producten
	$query_product = "SELECT * FROM product WHERE id = '$gegevens_bestellingen[2]'";
	$result_product = mysqli_query($connection, $query_product);
	$gegevens_product = mysqli_fetch_row($result_product);
	//columns -> id, naam, prijs

	
	
	
	
	
	
	
	
	
	require("fpdf.php");
	$pdf = new FPDF();
	// var_dump(get_class_methods($pdf));
	
	$pdf -> Addpage();
	
	// titel
	$pdf -> SetFont("Arial","I","20");
	$pdf -> Cell(0,10,"Garage de Appelboom",1,1,"C");
	
	$pdf -> SetFont("Arial","","8");
	$pdf -> write(5,"Factuur gemaakt op $gegevens_bestellingen[3] \n");
	
	//inhoud
	$pdf -> SetFont("Arial","","10");
	
	$pdf -> write(15,"Hieronder de facture t.a.v. de heer/mevrouw $gegevens_klanten[1] \n");
	
	$pdf -> Cell(30,5,"Kenteken",1,0);
	$pdf -> Cell(30,5,"Afspraak",1,0);
	$pdf -> Cell(30,5,"Monteur",1,0);
	$pdf -> Cell(30,5,"Afspraak datum",1,0);
	$pdf -> Cell(30,5,"Afspraak tijd",1,0);
	$pdf -> Cell(30,5,"Producten",1,0);
	
	
	$pdf -> write(5,"\n");
	
	$pdf -> Cell(30,5,"$gegevens_klanten[2]",1,0);
	$pdf -> Cell(30,5,"$gegevens_bestellingen[5]",1,0);
	$pdf -> Cell(30,5,"$gegevens_bestellingen[6]",1,0);
	$pdf -> Cell(30,5,"$gegevens_bestellingen[7]",1,0);
	$pdf -> Cell(30,5,"$gegevens_bestellingen[8]",1,0);
	$pdf -> Cell(30,5,"$gegevens_product[1]",1,0);
	
	$pdf -> write(15,"\n");
	
	$pdf -> Cell(40,5,"Diensten en Producten",1,0);
	$pdf -> Cell(30,5,"Prijzen",1,0);
	
	$pdf -> write(5,"\n");
	
	$pdf -> Cell(40,5,"$gegevens_bestellingen[5]",1,0);
	$pdf -> Cell(30,5,"",1,0);
	
	$pdf -> write(5,"\n");
	
	$pdf -> Cell(40,5,"$gegevens_product[1]",1,0);
	$pdf -> Cell(30,5,"$gegevens_product[2]",1,0);
	
	$pdf -> write(15,"\n");
	
	$pdf -> Cell(40,5,"Totale kosten",1,0);
	$pdf -> Cell(30,5,"$gegevens_bestellingen[4]",1,0);
	
	$pdf -> write(15,"\n");
	
	$pdf -> write(15,"Bedankt voor uw bestelling \n Met vriendelijke groet, \n Garage de Appelboom");
	
	$pdf -> Output();

?>