<?php

	//database verbindingen configueren
	$hostname = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_name = "GaragedeAB";
	
	//verbinden met database
	$connection = mysqli_connect($hostname, $db_user, $db_pass, $db_name);
	
	//valideer sessie
	session_start();
	if(isset($_SESSION['email'])){
		if(($_SESSION['email']) == "admin"){
			header("location: admin_home.php");
		} else {
		header("location: home.php");
		echo "<script>console.log('sessie actief');</script>";
		}
	} else {
		echo "<script>console.log('sessie onactief');</script>";
	}
	
	//inloggen
	if(isset($_POST['inloggen'])){		
           $email = mysqli_real_escape_string($connection, $_POST["email"]);  
           $wachtwoord = mysqli_real_escape_string($connection, $_POST["wachtwoord"]);  
           $wachtwoord = base64_encode($wachtwoord);
           $query = "SELECT * FROM klanten WHERE email = '$email' AND wachtwoord = '$wachtwoord'";  
           $result = mysqli_query($connection, $query);  
           if(mysqli_num_rows($result) > 0)  
           {  
				session_start();
                $_SESSION['email'] = $email;
                header("Location: home.php");  
				
		} else {
			echo "<script>alert('Ongeldige gegevens');</script>";
		}
	}
?>
<html>
	<head>
		<title>Garage de Appelboom</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Garage de Appelboom" >
		<meta name="keywords" content="autogarage, appelboom, reparatie, servicebeurt, service">
		<link rel="stylesheet" href="bootstrapreplace.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<link rel="shortcut icon" href="img/icon.png">
		<style>
		.container{
			border: 1px solid black;
			border-radius: 10px 10px 10px 10px;
			width:	40%;
			text-align:	center;
			background-color:	#ffffcc;
		}
		
		.button{
			width:	125px;
			height: 35px;
		}
		
		.veld{
			width:	200px;
		}
		
		.admin {
			height:	25px;
			width:	25px;
			border-radius:	10px 10px 10px 10px;
			background-color:	lightblue;
			position: absolute;
			bottom:	8px;
			right:	16px;
		}
		
		.admin:hover {
			height:	30px;
			width:	30px;
		}
		
		.admin:active {
			height:	25px;
			width:	25px;
		}
		
		.admintxt {
			position: absolute;
			bottom: 0px;
			right:	50px;
		
		</style>
	</head>
	<body>
		<!-- titel -->
		<h1 align="center">Garage de Appelboom</h1>
		
		<!-- inhoud -->
		<div class="container">
			<div class="row">
				<br>
				<form name="inloggen" enctype="multipart/form-data" method="POST" action="">
					<p>E-mail adres</p>
					<input type="email" class="veld" name="email" maxlength="50" required>
				
					<br><br>
				
					<p>Wachtwoord</p>
					<input type="password" class="veld" name="wachtwoord" maxlength="30" required>
				
					<br><br>
				
					<a href="herstel.php">
					<p>wachtwoord vergeten?</p>
					</a>
				
					<input type="submit" class="button" value="Inloggen" name="inloggen">
					<input type="button" class="button" value="Aanmelden" name="aanmelden" onclick="location.href='aanmelden.php'">
				</form>
				<br>
			</div>
		</div>
		
			<p class="admintxt">Admin Login</p>
			<a href="admin.php">
				<img class="admin" src="img/admin.png" align="right">
			</a>
			
	</body>
</html>