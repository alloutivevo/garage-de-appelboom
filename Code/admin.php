<?php

	//database verbindingen configueren
	$hostname = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_name = "GaragedeAB";
	
	//verbinden met database
	$connection = mysqli_connect($hostname, $db_user, $db_pass, $db_name);
	
	//inloggen
	if(isset($_POST['inloggen'])){		
           $email = mysqli_real_escape_string($connection, $_POST["username"]);  
           $wachtwoord = mysqli_real_escape_string($connection, $_POST["wachtwoord"]);  
           $wachtwoord = base64_encode($wachtwoord);
           $query = "SELECT * FROM klanten WHERE email = '$email' AND wachtwoord = '$wachtwoord'";  
           $result = mysqli_query($connection, $query);  
           if(mysqli_num_rows($result) > 0)  
           {  
				session_start();
                $_SESSION['email'] = $email;
                header("Location: admin_home.php");
				
		} else {
			echo "<script>alert('Ongeldige gegevens');</script>";
		}
	}
?>
<html>
	<head>
		<title>Garage de Appelboom</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Garage de Appelboom" >
		<meta name="keywords" content="autogarage, appelboom, reparatie, servicebeurt, service">
		<link rel="stylesheet" href="bootstrapreplace.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		<link rel="shortcut icon" href="img/icon.png">
		<style>
		.container{
			border: 1px solid black;
			border-radius: 10px 10px 10px 10px;
			width:	40%;
			text-align:	center;
			background-color:	lightblue;
		}
		
		.button{
			width:	125px;
			height: 35px;
		}
		
		.veld{
			width:	200px;
		}
		</style>
	</head>
	<body>
		<!-- titel -->
		<h1 align="center">Garage de Appelboom</h1>
		
		<!-- inhoud -->
		<div class="container">
			<div class="row">
			
				<h3 align="center">Administrator Login </h3>
				
				<br>
				<form name="inloggen" enctype="multipart/form-data" method="POST" action="">
					<p>Gebruikersnaam</p>
					<input type="text" class="veld" name="username" maxlength="10" required>
				
					<br><br>
				
					<p>Wachtwoord</p>
					<input type="password" class="veld" name="wachtwoord" maxlength="30" required>
				
					<br><br>
				
					<input type="submit" class="button" value="Inloggen" name="inloggen">
					<input type="button" class="button" value="Terug" name="terug" onclick="location.href='index.php'">
				</form>
				<br>
				
			</div>
		</div>			
	</body>
</html>